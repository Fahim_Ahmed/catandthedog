// Fahim Ahmed

#include "ofMain.h"
#include "ofxSimpleHttp.h"
#include "ofxXmlSettings.h"

#define BASE_PHOTO "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=8cc4e7e51b9b0f8adf7f807c0547d0c3&format=rest"
#define TAGS "&tags='mountains,building,nature,sea,sun,moon,fog,river,skyscraper,night,sunrise,dawn,dusk,fantasy'"

enum URL_RETURN_TYPE {
	PLACES = 0,
	PHOTOS = 1,
	IMAGE = 2
};

class PhotoLoader : public ofBaseApp {
private:
	ofxSimpleHttp http;
	ofxXmlSettings xml_places;
	ofxXmlSettings xml_photos;

	URL_RETURN_TYPE urlType;

	vector<int> randomValues;
	string searchTerm;

public:
	ofEvent<string> event;
	ofEvent<string> eventImage;

	string urlPlaces;
	string urlPhotos;

	PhotoLoader(){
		ofxSimpleHttp::createSslContext();
		http.setVerbose(true);
		ofAddListener(http.httpResponse, this, &PhotoLoader::responseCallback);
	}

	void PhotoLoader::responseCallback(ofxSimpleHttpResponse &response){
		if(response.status == 200){
			if(urlType == PLACES){
				processPlacesXml(response);
			}else if(urlType == PHOTOS){
				processPhotoXml(response);
			}else if(urlType == IMAGE){
				ofDirectory dir;
				dir.createDirectory("flickr");

				ofDirectory f;
				f.open( response.absolutePath);
				if( f.exists() ){
					f.moveTo("flickr/" + response.fileName);
				}else{
					cout << "file was not downloaded???!" << endl;
					string str = "Download failed";
					ofNotifyEvent(event, str, this);
				}

				string path = "flickr/" + response.fileName;				
				ofNotifyEvent(eventImage, path, this);

				cout << response.url << endl;
			}
		}else{
			cout << "epic failed: " << response.reasonForStatus << endl;

			string str = "Invalid location or error";
			ofNotifyEvent(event, str, this);
		}
	}

	void fetchLocation(string loc, URL_RETURN_TYPE t = PLACES){
		if(loc != searchTerm)
			randomValues.clear();

		urlPlaces = getLocationUrl(loc);
		http.fetchURL(urlPlaces, true);
		urlType = t;

		searchTerm = loc;
	}

	string getXmlUrl(string _lat, string _lon, string _woe_id){
		string xtra = "&page=1&per_page=250&extras=url_h&license='1,2,3,4,5,6,7'&content_type=1&sort=interestingness-desc&radius=32";
		xtra = xtra + TAGS;

		string lat = "&lat=" + _lat;
		string lon = "&lon=" + _lon;
		string woe = "&woe_id=" + _woe_id;

		string u = BASE_PHOTO + lat + lon + woe + xtra;

		//cout << u << endl;

		return u;

	}

	void processPlacesXml(ofxSimpleHttpResponse &response){
		xml_places.loadFromBuffer(response.responseBody);
		int length = xml_places.getAttribute("rsp:places", "total", -1);

		cout << "result: " << length << endl;

	//	string str = "Search Image";
	//	ofNotifyEvent(event, str, this);

		if(length > 0){
			string lat = xml_places.getAttribute("rsp:places:place", "latitude", "-1", 0);
			string lon = xml_places.getAttribute("rsp:places:place", "longitude", "-1", 0);
			string woe = xml_places.getAttribute("rsp:places:place", "woeid", "-1", 0);

			urlPhotos = getXmlUrl(lat, lon, woe);
			urlType = PHOTOS;

			http.fetchURL(urlPhotos, true);
		}else{
			cout << "epic failed places" << endl;
			string str = "Invalid location or error";
			ofNotifyEvent(event, str, this);
		}
	}

	void processPhotoXml(ofxSimpleHttpResponse &response){	
		xml_photos.clear();
		xml_photos.loadFromBuffer(response.responseBody);

		xml_photos.pushTag("rsp");
		xml_photos.pushTag("photos");

		int length = xml_photos.getNumTags("photo");

		//int length = xml_photos.getAttribute("rsp:photos", "total", -1);		
		//length = (length > 250) ? 250 : length;

		cout << "result: " << length << endl;

		if(length > 0){

			//<photo id="7285811690" owner="7749932@N08" secret="dee0de4f6b" server="7089" 
			//farm="8" title="Light and shade" ispublic="1" isfriend="0" isfamily="0" 
			//url_h="https://farm8.staticflickr.com/7089/7285811690_e7aa767144_h.jpg" 
			//height_h="1066" width_h="1600"/>
			string str = "Loading...";
			ofNotifyEvent(event, str, this);

			urlType = IMAGE;

			int r = ofRandom(length);
			while(existsThis(r)) 
				r = ofRandom(length);

			if(xml_photos.attributeExists("photo", "url_h", r)){
				string url = xml_photos.getAttribute("photo", "url_h", "-1", r);
				http.fetchURLToDisk(url, true);
				xml_photos.popTag();
				xml_photos.popTag();
				return;
			}

			string farm = xml_photos.getAttribute("photo", "farm", "8", r);
			string id = xml_photos.getAttribute("photo", "id", "7285811690", r);
			string secret = xml_photos.getAttribute("photo", "secret", "dee0de4f6b", r);
			string server = xml_photos.getAttribute("photo", "server", "7089", r);
			string h = "";

			string url = getPhotoUrl(farm, server, id, secret, h);
			http.fetchURLToDisk(url, true);

		}else{
			cout << "epic failed photos: " << response.reasonForStatus << endl;
			string str = "Invalid location or error";
			ofNotifyEvent(event, str, this);
		}

		xml_photos.popTag();
		xml_photos.popTag();
	}

	void update(){
		http.update();
	}

	string getPhotoUrl(string farm, string server, string id, string secret, string size = ""){
		//https://farm6.staticflickr.com/5057/5452536111_3950a3edac.jpg

		string url = "https://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret +""+ size + ".jpg";
		//cout << url << endl;

		return url;
	}

	string getLocationUrl(string loc){		
		ofStringReplace(loc, " ", "%20");
		loc = "'" + loc + "'";

		string place_url = "https://api.flickr.com/services/rest/?method=flickr.places.find&api_key=8cc4e7e51b9b0f8adf7f807c0547d0c3&query=" + loc;
		return place_url;
	}

	void exit(){
		ofRemoveListener(http.httpResponse, this, &PhotoLoader::responseCallback);
		ofxSimpleHttp::destroySslContext();
	}

	bool existsThis(int r){
		if(randomValues.size() >= 250){
			randomValues.clear();
			randomValues.push_back(r);
			return false;
		}
			

		for (int i = 0; i< randomValues.size(); i++){
			int x = randomValues[i];
			if(x == r)
				return true;
		}
		
		randomValues.push_back(r);
		return false;
	}
};