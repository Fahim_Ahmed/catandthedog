// Fahim Ahmed

#pragma once

#include "ofMain.h"
#include "ofxOBJModel.h"

class OddGeometry : public ofBaseApp {
public:
	OddGeometry();
	void setValues(int offset, float uMin, float uMax, float vMin, int vMax, int distortion, int uJitter, int vJitter, int uChance, int vChance);
	void setValues(int xCount, int yCount);
	void draw(bool bPolyRenderMode, bool bDrawMode, bool bShowTex);
	void calculate();
	void setTexture(ofImage &t);
	void setArg(float a);
	void saveModel();

private:
	//Mesh Geometry Params
	int xCount;
	int yCount;
	int offset;	
	int vMax;
	int distortion;
	int uJitter;
	int vJitter;
	int uChance;
	int vChance;

	float uMin;
	float uMax;
	float vMin;

	//Mesh Color Params - hue, sat, brightness
	int vh;
	int vs;
	int vb;

	//etc
	float params;
	float params2;
	float arg;

	bool _saveMode;

	ofImage tex;

	vector<vector<ofVec3f>> vertices;
	vector<vector<ofVec3f>> normals;
	vector<vector<ofVec3f>> finalVertices;

	ofxOBJModel model;
};