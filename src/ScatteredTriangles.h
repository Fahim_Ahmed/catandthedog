// Fahim Ahmed

#include "ofMain.h"
#include "ofxOBJModel.h"

class ScatteredTriangle : public ofThread {

public:

	ScatteredTriangle();

	void threadedFunction();
	void setValues(int c, int xn, int yn, int zn, int xr, int yr, int zr);
	void calculateVertices(ofPrimitiveMode mode, bool lissejous);
	void calculateVertices(ofPrimitiveMode mode);
	void draw(ofPolyRenderMode mode);
	void addImage(ofImage &img);
	void removeImage();
	int getNumVertices();
	void saveModel();

private:
	int count;
	int xVar;
	int zVar;
	int yVar;
	float xRange;
	float yRange;
	float zRange;
	bool bShowImageColor;

	ofImage img;
	vector<ofVec3f> vertices;
	ofMesh _mesh;
};