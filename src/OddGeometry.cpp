// Fahim Ahmed
// very odd -.-

#include "OddGeometry.h"

OddGeometry::OddGeometry(){
	//Mesh Geometry Params
	xCount = 60;
	yCount = xCount;
	offset = 50;
	uMin = -PI;
	uMax = PI;
	vMin = -PI;
	vMax = 30;
	distortion = 0;
	uJitter = 0;
	vJitter = 0;
	uChance = 40;
	vChance = 40;

	//Mesh Color Params - hue, sat, brightness
	vh = 190;
	vs = 90;
	vb = 85;

	params = 1;
	params2 = 1;

	vertices.clear();
	vertices.resize(yCount + 2, vector<ofVec3f>(xCount + 2));

	normals.clear();	
	normals.resize(yCount + 2, vector<ofVec3f>(xCount + 2));

	_saveMode = false;
}

void OddGeometry::setArg(float a){
	this->arg = a;
}

void OddGeometry::calculate(){
	for (int j = 0; j <= yCount+1; j++){
		for (int i = 0; i <= xCount+1; i++){

			float u = ofMap(i, 0, xCount, uMin, uMax);
			float v = ofMap(j, 0, yCount, vMin, vMax);

			//screw
			/*float x = u * cos(v);
			float y = u * sin(v);
			float z = v * cos(u);*/

			//cork screw
		/*	float x = cos(u) * cos(v);
			float y = sin(u) * cos(v);
			float z = sin(v) + params * u;*/

			//sphere
			/*float x = 2 * (sin(v) * sin(u));
			float y = 2 * (params * cos(v));
			float z = 2 * (sin(v) * cos(u));*/

			//figure 8 torus
			/*float x = 1.5f * cos(u)	* (params + sin(v) * cos(u) - sin(2 * v) * sin(u) / 2);
			float y = 1.5f * sin(u)	* (params + sin(v) * cos(u) - sin(2 * v) * sin(u) / 2);
			float z = 1.5f * sin(u) * sin(v) + cos(u) * sin(2 * v) / 2;*/

			//Trianguloid
			/*params = 0.75 + arg;
			float x = 0.75f * (sin(3 * u) * 2 / (2 + cos(v)));
			float y = 0.75f * ((sin(u) + 2 * params * sin(2 * u)) * 2 / (2 + cos(v + TWO_PI)));
			float z = 0.75f * ((cos(u) - 2 * params * cos(2 * u)) * (2 + cos(v)) * ((2 + cos(v + TWO_PI / 3)) * 0.25f));*/

			//Modified Trianguloid
			params2 = sin(u) * arg;			//0-2
			params = 0.75 + arg; //cos(u);			// 0.5-2

			float x = 0.75f * (sin(3 * u) * 2 / (1.5 + cos(v)));
			float y = 0.75f * ((sin(u) + 2 * params2 * sin(2 * u)) * 2 / (2 + sin(v + TWO_PI)));
			float z = 0.75f * ((cos(u) - 2 * params * cos(2 * u)) * (2 + cos(v)) * ((2 + cos(v + TWO_PI / 3)) * 0.25f));

			vertices[j][i] = ofVec3f(x * offset, y * offset, z * offset);
		}
	}

	ofSeedRandom(404);

	bool uChange = false;	
	bool vChange = false;

	for (int j = 0; j <= yCount+1; j++){
		float p = ofRandom(100);

		if(p < uChance) uChange = true;
		else uChange = false;

		p = ofRandom(100);

		if(p < vChance) vChange = true;
		else vChange = false;

		ofVec3f uDir(ofRandom(-uJitter, uJitter), ofRandom(-uJitter, uJitter), ofRandom(-uJitter, uJitter));
		ofVec3f vDir(ofRandom(-vJitter, vJitter), ofRandom(-vJitter, vJitter), ofRandom(-vJitter, vJitter));

		for (int i = 0; i <= xCount+1; i++){
			if(uChange){		
				ofVec3f v = vertices[i][j];
				v += uDir;
				vertices[i][j] = v;
			}	

			if(vChange){		
				ofVec3f v = vertices[j][i];
				v += vDir;
				vertices[j][i] = v;
			}

			if(j <= yCount && i <= xCount){				
				ofVec3f va = vertices[j][i];
				ofVec3f vb = vertices[j+1][i];
				ofVec3f vc = vertices[j][i+1];

				ofVec3f e1 = va - vb;
				ofVec3f e2 = va - vc;
				ofVec3f no = e2.cross(e1);

				normals[j][i] = no;
			}
		}
	}
}

void OddGeometry::draw(bool bPolyRenderMode, bool bDrawMode, bool bShowTex){
	ofEnableNormalizedTexCoords();	
	//ofEnableDepthTest();
	//ofEnableAlphaBlending();

	ofPushMatrix();
	//ofScale(0.8, 0.8, 0.8);

	ofSetColor(255);
	if(bShowTex && tex.isAllocated()) tex.bind();
	//ofEnableAlphaBlending();

	//glShadeModel(GL_FLAT);
	if(!bPolyRenderMode) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	ofSeedRandom(125);

	for (int j = 0; j <= yCount; j++){

		if(!bShowTex){
			ofColor c;
			int hue = ofMap(ofRandom(vh-2,vh+2), 0, 360, 0, 255);
			int sat = ofMap(ofRandom(vs-5,vs+5), 0, 100, 0, 255);
			int bri = ofMap(ofRandom(vb - 5, vb + 5), 0, 100, 0, 255);

			c.setHsb(hue, sat, bri);
			ofSetColor(c);
		}

		if(bDrawMode)
			glBegin(GL_QUAD_STRIP);
		else glBegin(GL_QUADS);

		ofxOBJGroup group;
		ofVec3f lastV0, lastV1;

		for (int i = 0; i <= xCount; i++){

			float r1 = distortion * ofRandom(-1, 1);
			float r2 = distortion * ofRandom(-1, 1);
			float r3 = distortion * ofRandom(-1, 1);

			ofVec3f no = normals[j][i];

			float ox = ofMap(i, 0, xCount * 1, 0, 1);
			float oy = ofMap(j, 0, yCount * 1, 1, 0);

			glTexCoord2f(ox, oy);
			glNormal3f(no.x, no.y, no.z);
			glVertex3f(vertices[j][i].x + r1, vertices[j][i].y + r2, vertices[j][i].z + r3);
			//glTexCoord2f(ox, oy + off);
			glVertex3f(vertices[j+1][i].x + r1, vertices[j+1][i].y + r2, vertices[j+1][i].z + r3);

			if(_saveMode){

				if(i % 2 == 0){
					ofVec3f v0 = ofVec3f(vertices[j][i].x + r1, vertices[j][i].y + r2, vertices[j][i].z + r3);
					ofVec3f v1 = ofVec3f(vertices[j+1][i].x + r1, vertices[j+1][i].y + r2, vertices[j+1][i].z + r3);

					finalVertices[j][i] = v0;
					finalVertices[j][i+1] = v1;
				}
			}

			//GL_TRIANGLE_STRIPE
			//	glVertex3f(vertices[j+1][i+1].x + r1, vertices[j+1][i+1].y + r2, vertices[j+1][i+1].z + r3);
			//	glVertex3f(vertices[j][i+1].x + r1, vertices[j][i+1].y + r2, vertices[j][i+1].z + r3);
		}

		//if(_saveMode) model.addGroup(group);

		glEnd();
	}

	//glBindTexture(tex.getTextureData().textureTarget, 0);
	//glDisable(tex.getTextureData().textureTarget);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

//	ofDisableAlphaBlending();
	if(bShowTex && tex.isAllocated()) tex.unbind();

	ofPopMatrix();

	//ofDisableAlphaBlending();
//	ofDisableDepthTest();
	ofDisableNormalizedTexCoords();
}

void OddGeometry::setTexture(ofImage &t){
	this->tex = t;
}

void OddGeometry::saveModel(){
	model.clear();
	finalVertices.clear();

	finalVertices.resize(yCount + 2, vector<ofVec3f>(xCount + 4));

	_saveMode = true;
	draw(true, true, true);
	_saveMode = false;

	ofVec3f lv0, lv1;

	for (int j = 0; j <= yCount; j++){
		
		ofxOBJGroup group;

		for (int i = 0; i <= xCount; i+=4){
			ofxOBJFace face;

			face.addVertex(finalVertices[j][i]);
			face.addVertex(finalVertices[j][i+1]);
			face.addVertex(finalVertices[j][i+3]);
			face.addVertex(finalVertices[j][i+2]);
			
			group.addFace(face);

			if(i>0){
				ofxOBJFace face;
			
				face.addVertex(lv0);
				face.addVertex(lv1);
				face.addVertex(finalVertices[j][i+1]);
				face.addVertex(finalVertices[j][i]);
				
				group.addFace(face);
			}

			lv0 = finalVertices[j][i+2];
			lv1 = finalVertices[j][i+3];
		}

		model.addGroup(group);
	}

	model.save("3d/odd_" + ofToString(ofGetUnixTime()) + ".obj");

	cout << "obj write completed." << endl;
}


void OddGeometry::setValues(int offset, float uMin, float uMax, float vMin, int vMax, int distortion, int uJitter, int vJitter, int uChance, int vChance){
	this->offset = offset;
	this->uMin = uMin;
	this->uMax = uMax;
	this->vMin = vMin;
	this->vMax = vMax;
	this->distortion = distortion;
	this->uJitter = uJitter;
	this->vJitter = vJitter;
	this->uChance = uChance;
	this->vChance = vChance;
}

void OddGeometry::setValues(int xCount, int yCount){
	this->xCount = xCount;
	this->yCount = xCount;		//need to fix

	vertices.clear();	
	vertices.resize(yCount + 2, vector<ofVec3f>(xCount + 2));

	normals.clear();	
	normals.resize(yCount + 2, vector<ofVec3f>(xCount + 2));
}