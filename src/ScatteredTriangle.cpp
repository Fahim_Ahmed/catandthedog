// Fahim Ahmed

#include "ScatteredTriangles.h"

//#define FOR_ALL_VERTICES for(int i = 0; i < _mesh.getNumVertices(); i++)

ScatteredTriangle::ScatteredTriangle(){
	count = 100;
	xVar = 10;
	yVar = 10;
	zVar = 10;

	xRange = 200;
	yRange = 200;
	zRange = 200;

	bShowImageColor = false;
}

void ScatteredTriangle::threadedFunction() {
	while(isThreadRunning()){			
		calculateVertices(OF_PRIMITIVE_TRIANGLE_STRIP);
		draw(OF_MESH_FILL);

		float ms = ofGetLastFrameTime();
		ofSleepMillis(ms);
	}
}

void ScatteredTriangle::setValues(int c, int xn, int yn, int zn, int xr, int yr, int zr){
	this->count = c;

	this->xVar = xn;
	this->yVar = yn;
	this->zVar = zn;

	this->xRange = xr;
	this->yRange = yr;
	this->zRange = zr;
}

void ScatteredTriangle::calculateVertices(ofPrimitiveMode mode, bool lissejous){
	//	//Lissejous model
	//x(t) = a1 * cos (k1 * t + l1 )	l1 = 0;
	//y(t) = a2 * cos (k2 * t + l2 )	l2 = PI/2;
	//z(t) = a3 * cos (k3 * t + l3 )	l3 = PI/2;

	// k = 3, 5, 2
	float k1 = 1;
	float k2 = 1;
	float k3 = 3;

	vertices.clear();
	_mesh.clearVertices();
	_mesh.clearIndices();
	_mesh.clearColors();

	_mesh.setMode(mode);

	int iw = img.getWidth();
	int ih = img.getHeight();
	ofColor c;

	ofSeedRandom(101);

	float dt = TWO_PI / count;
	float t = 0;

	for(int i = 0; i < count; i++){		
		float _x = ofRandomf() * xRange * cos(k1 * t + 0);
		float _y = ofRandomf() * yRange * cos(k2 * t + HALF_PI);
		float _z = ofRandomf() * zRange * cos(k3 * t + HALF_PI);
		
		t += dt;

		int vx = _x + ofRandom(-xVar, xVar);
		int vy = _y + ofRandom(-yVar, yVar);
		int vz = _z + ofRandom(-zVar, zVar);
		ofVec3f _p1(vx, vy, vz);

		if(bShowImageColor){
			float ix = ofMap(vx, -(xVar + xRange), xVar + xRange, 0, iw-1);
			float iy = ofMap(vy, -(yVar + yRange), yVar + yRange, 0, ih-1);
			c = img.getColor(ix, iy);
			_mesh.addColor(c);
		}

		vx = _x + ofRandom(-xVar, xVar);
		vy = _y + ofRandom(-yVar, yVar);
		vz = _z + ofRandom(-zVar, zVar);
	
		ofVec3f _p2(vx, vy, vz);

		if(bShowImageColor){
			float ix = ofMap(vx, -(xVar + xRange), xVar + xRange, 0, iw-1);
			float iy = ofMap(vy, -(yVar + yRange), yVar + yRange, 0, ih-1);
			c = img.getColor(ix, iy);
			_mesh.addColor(c);
		}

		vx = _x + ofRandom(-xVar, xVar);
		vy = _y + ofRandom(-yVar, yVar);
		vz = _z + ofRandom(-zVar, zVar);

		ofVec3f _p3(vx, vy, vz);

		if(bShowImageColor){
			float ix = ofMap(vx, -(xVar + xRange), xVar + xRange, 0, iw-1);
			float iy = ofMap(vy, -(yVar + yRange), yVar + yRange, 0, ih-1);
			c = img.getColor(ix, iy);
			_mesh.addColor(c);
		}

		_mesh.addVertex(_p1);
		_mesh.addVertex(_p2);
		_mesh.addVertex(_p3);

		vertices.push_back(_p1);
		vertices.push_back(_p2);
		vertices.push_back(_p3);
	}

	_mesh.setupIndicesAuto();
}

void ScatteredTriangle::calculateVertices(ofPrimitiveMode mode){
	vertices.clear();
	_mesh.clearVertices();
	_mesh.clearIndices();
	_mesh.clearColors();

	_mesh.setMode(mode);

	int iw = img.getWidth();
	int ih = img.getHeight();
	ofColor c;

	ofSeedRandom(101);

	for(int i = 0; i < count; i++){
		int x = ofRandom(-xRange, xRange);
		int y = ofRandom(-yRange, yRange);
		int z = ofRandom(-zRange, zRange);

		//x = x * sin(ofMap(i, 0, count, 0, HALF_PI));
		//y = y * cos(ofMap(i, 0, count, 0, HALF_PI));
		//z = z * cos(ofMap(i, 0, count, 0, HALF_PI));

		int vx = x + ofRandom(-xVar, xVar);
		int vy = y + ofRandom(-yVar, yVar);
		int vz = z + ofRandom(-zVar, zVar);
		ofVec3f _p1(vx, vy, vz);

		if(bShowImageColor){
			float ix = ofMap(vx, -(xVar + xRange), xVar + xRange, 0, iw-1);
			float iy = ofMap(vy, -(yVar + yRange), yVar + yRange, 0, ih-1);
			c = img.getColor(ix, iy);
			_mesh.addColor(c);
		}

		vx = x + ofRandom(-xVar, xVar);
		vy = y + ofRandom(-yVar, yVar);
		vz = z + ofRandom(-zVar, zVar);
		ofVec3f _p2(vx, vy, vz);

		if(bShowImageColor){
			float ix = ofMap(vx, -(xVar + xRange), xVar + xRange, 0, iw-1);
			float iy = ofMap(vy, -(yVar + yRange), yVar + yRange, 0, ih-1);
			c = img.getColor(ix, iy);
			_mesh.addColor(c);
		}

		vx = x + ofRandom(-xVar, xVar);
		vy = y + ofRandom(-yVar, yVar);
		vz = z + ofRandom(-zVar, zVar);
		ofVec3f _p3(vx, vy, vz);

		if(bShowImageColor){
			float ix = ofMap(vx, -(xVar + xRange), xVar + xRange, 0, iw-1);
			float iy = ofMap(vy, -(yVar + yRange), yVar + yRange, 0, ih-1);
			c = img.getColor(ix, iy);
			_mesh.addColor(c);
		}

		_mesh.addVertex(_p1);
		_mesh.addVertex(_p2);
		_mesh.addVertex(_p3);

		vertices.push_back(_p1);
		vertices.push_back(_p2);
		vertices.push_back(_p3);
	}

	_mesh.setupIndicesAuto();
}

void ScatteredTriangle::draw(ofPolyRenderMode mode){
	_mesh.draw(mode);
}

void ScatteredTriangle::saveModel(){
	ofxOBJModel model;
	ofxOBJGroup group;

	for(int i = 0; i < _mesh.getNumVertices(); i+=3){

		if(i > 0){
			ofxOBJFace face;

			face.addVertex(_mesh.getVertex(i-2));
			face.addVertex(_mesh.getVertex(i-1));
			face.addVertex(_mesh.getVertex(i));
			face.calculateFlatNormals();

			group.addFace(face);
		}

		ofxOBJFace face;

		face.addVertex(_mesh.getVertex(i));
		face.addVertex(_mesh.getVertex(i+1));
		face.addVertex(_mesh.getVertex(i+2));		
		face.calculateFlatNormals();

		group.addFace(face);
	}

	model.addGroup(group);
	model.save("3d/tris_" + ofToString(ofGetUnixTime()) + ".obj");

	cout << "obj write completed." << endl;
}

void ScatteredTriangle::addImage(ofImage &img){
	this->img = img;
	bShowImageColor = true;
}

void ScatteredTriangle::removeImage(){
	bShowImageColor = false;
}

int ScatteredTriangle::getNumVertices(){
	return _mesh.getNumVertices();
}