#include "ofMain.h"
#include "testApp.h"
#include "ofAppGLFWWindow.h"

//#define FULL_SCREEN

//========================================================================
int main( ){
	//ofSetCurrentRenderer(ofGLProgrammableRenderer::TYPE);
	ofAppGLFWWindow window;

#ifdef FULL_SCREEN
	ofSetupOpenGL(&window, 1920, 1080, OF_FULLSCREEN);
#else
	ofSetupOpenGL(&window, 1600, 900, OF_WINDOW);
#endif

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp(new testApp());
}