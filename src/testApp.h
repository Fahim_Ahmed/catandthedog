// Fahim Ahmed
// I've a dream. A dream, one day i'll properly comment my codes.
// For now, pragma once.

#pragma once

#include "ofMain.h"
#include "ofxPostProcessing.h"
#include "ofxUI.h"
#include "ofxRemoteUIServer.h"
#include "ScatteredTriangles.h"
#include "ofxThreadedImage.h"
#include "ofxTimeMeasurements.h"
#include "ofxXmlSettings.h"
#include "PhotoLoader.h"
//#include "ofxMovieExporter.h"
#include "ofxCameraFilter.h"
#include "OddGeometry.h"
#include "ofxTileSaver.h"
#include "UIGrid.h"

class testApp : public ofBaseApp{
	private:
		ofxThreadedImage screenImage;
		ofxTileSaver *highresScreen;
		ofImage trisImage;

		ScatteredTriangle tris;

		ofEasyCam cam, scam1, scam2;
		ofLight light;		
		ofFloatColor colorLight;

		OddGeometry geom;
		ofImage tex;		
		ofMaterial mat;

		vector<ofVec3f>	points;

		ofxPostProcessing postfx;
		KaleidoscopePass::Ptr kaleido;

		ofxUICanvas *gui_local;
		ofxUICanvas *gui_web;
		ofxUICanvas *gui_music;
		ofxUITextInput* txt_location;
		ofxUILabelButton *btn_web;
		ofxUILabelButton *btn_music;

		//Apex::ofxMovieExporter vidRecorder;

		ofxCameraFilter camFilter;

		PhotoLoader loader;
		UIGrid *grid;
		ofxXmlSettings xParams;
		ofTrueTypeFont font;

		//Mesh Geometry Params
		int xCount;
		int yCount;
		int offset;
		float uMin;
		float uMax;
		float vMin;
		float vMax;
		float distortion;
		float uJitter;
		float vJitter;

		//Mesh Color Params - hue, sat, brightness
		int vh;
		int vs;
		int vb;

		//Fill background
		int blackness;
		int alpaa;

		//Gradient background
		ofColor colorDarkBlue;
		ofColor colorBlue;

		//Tris paramas
		float	numTris;
		float	xVar;
		float	yVar;
		float	zVar;
		float	xRange;
		float	yRange;
		float	zRange;
		float	uChance;
		float	vChance;

		//Mat params
		int shininess;
		float specularIntensity;

		//Music params
		ofSoundPlayer music;
		int		numBand;
		bool	bSampled;
		float	*fft;
		float	*fftSmoothed;
		float	avgLow;
		float	avgMid;
		float	avgHigh;

		//etc bools
		bool bBlendADD;
		bool bBlendMultiply;
		bool bShowTex;
		bool bDrawMode;			//fill vs wireframe mode
		bool bPolyRenderMode;	//triangles vs triangle_stripe mode
		bool drawTrisStripe;	//tris stripe
		bool bKaleido;			//mesmerizing
		bool bDarkBg;			//dark vs light bg

		int kaleidoSegments;

		//functions
		void initVariables();
		void initPostFx();
		void setNormals(ofMesh &mesh);
		void drawMain();
		void updateMesh();
		void wobblyMove();
		void subViewport();
		void processOpenedFile(ofFileDialogResult result);
		void guiEvent(ofxUIEventArgs &e);
		void openFile();
		void responseCallback(string &e);
		void setWebPhoto(string &path);
		void searchLocation(string loc);
		void initGUIElements();
		void initMusic();
		void rotateCam(ofEasyCam &camera, float xRot, float yRot, float zRot);

	public:
		void setup();
		void update();
		void draw();
		
//		void haveBeenNotified(string &s);

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		void setupRemoteNodes();
		void exit();
};
