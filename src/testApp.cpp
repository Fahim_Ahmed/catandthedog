// Fahim Ahmed
// there might be (definitely) some useless variables rolling around. Good luck future!

#include "testApp.h"

//#define DEBUG_TIMER
#define COLOR_DARK_GRAY_BLUE 30, 33, 46
#define COLOR_GRAY_BLUE 67, 72, 89

#define UI_BUTTON_LOCAL "LOAD IMAGE"
#define UI_BUTTON_WEB "SEARCH"
#define UI_BUTTON_MUSIC "LOAD MUSIC"

//#define USE_FILTERS

float maxVal = 0;
float mean = 0;
float step;
float _time = 0;
float _rotation = 0;


//wobbly move
int _count = 0;
bool speedUp = false;
float dest = 0;


float params = 1, args = 1;
float params2 = 1, args2 = 1;

int sw, sh;

NoiseWarpPass::Ptr noiseWarp;
bool bPause = false;

bool bSearching = false;
bool bLocalImg = false;
bool bLocationImg = false;
bool bMusic = false;
bool bOpenMusic = false;
bool bGenerated = false;
bool bProcessing = false;
bool bShotTaken = false;
bool bTakeShot = false;

int nStep = 0;
int spell = 0;

float tmp = 0;
float mSlice = 10 * 1000;
float mStartPos = 0;

map<string, float> endValues;

//--------------------------------------------------------------
void testApp::setup(){
	ofSetWindowPosition((1920-ofGetWidth()) * 0.5, (1080 - ofGetHeight()) * 0.5);
	ofSetFrameRate(60);	
	ofBackground(0);
	ofSetGlobalAmbientColor(0);
	ofSetVerticalSync(true);
	
	font.loadFont("Gotham-Book.otf", 11, true, true);

	sw = ofGetWidth();
	sh = ofGetHeight();	

	TIME_SAMPLE_SET_FRAMERATE( 60.0f );
	TIME_SAMPLE_SET_DRAW_LOCATION( TIME_MEASUREMENTS_TOP_LEFT);
	TIME_SAMPLE_SET_AVERAGE_RATE(0.1);
	TIME_SAMPLE_SET_REMOVE_EXPIRED_THREADS(true);

	#ifndef DEBUG_TIMER
	TIME_SAMPLE_SET_ENABLED(false);
	#endif // DEBUG_TIMER
	
	initPostFx();		//prepare for drama
	initVariables();
	initMusic();
	initGUIElements();

	screenImage.allocate(sw, sh, OF_IMAGE_COLOR);
	//vidRecorder.resetPixelSource();
	//vidRecorder.setup(sw, sh, 8000000, 30, CODEC_ID_MPEG4, "mp4");	

	camFilter.setup(sw, sh);
	
	highresScreen = new ofxTileSaver();
	highresScreen->init(5, 0, true);

	grid = new UIGrid(sw - sh * 0.5 - 8, sh);
}

void testApp::initPostFx(){
	postfx.init(sw - sh * 0.5, sh, false);
	
	//postfx.createPass<BleachBypassPass>();
	//postfx.createPass<BloomPass>();
	noiseWarp = postfx.createPass<NoiseWarpPass>();
	kaleido = postfx.createPass<KaleidoscopePass>();	// Rorschach ;.;
	postfx.createPass<FxaaPass>();
	
	noiseWarp->setSpeed(0.1);
	noiseWarp->setEnabled(false);

	spell = ofRandom(6);

	string str("spells/10" + ofToString(spell));
	str += ".xml";
	xParams.loadFile(str);

	cout << "Spell: " << spell << endl;
}

//--------------------------------------------------------------
void testApp::update(){
	loader.update();
	ofSoundUpdate();

	fft = ofSoundGetSpectrum(numBand);
	step = 0.1;

	for(int i = 0;i < numBand; i++){
		if(maxVal < fft[i]) maxVal = fft[i];

		fft[i] *= step;
		step += (i > 32) ? 1 : 0.1;

		if(fft[i] > 0.75)
			fft[i] = 0.75;

		mean += fft[i];
	}

	//if(!isSampled) _time += ofGetLastFrameTime();
	_time += ofGetLastFrameTime();
	if(!bSampled && music.getIsPlaying() && _time >= 0.5f){
		_time = 0;
		avgHigh = 0;
		avgMid = 0;
		avgLow = 0;

		//isSampled = true;
		int div = numBand / 3;

		for(int i = 0; i < numBand; i++){
			if(i < div){
				avgHigh += fft[i];
			}else if(i < div * 2){
				avgMid += fft[i];
			}else if(i >= div * 2){
				avgLow += fft[i];				
			}
		}

		//bSampled = true;			
		avgHigh /= div;
		avgMid /= div;
		avgLow /= (numBand - div * 2);

		avgHigh *= 10;
		avgMid *= 10;
		avgLow *= 10;

		//distortion = ofMap(avgHigh, 0.0, 1.5, 30, 100, true); //30 - 70
		//numTris = ofMap(avgMid, 0.0, 1.5, 30, 900, true); //30 - 200
		//_rotation = ofMap(avgLow, 0.0, 2.0, 0, 180, true);
		//vMax = ofMap(avgLow, 0.0, 0.5, 15, 40);

		cout << avgHigh << " " << avgMid << " " << avgLow << endl; 
	//	cout << distortion << " " << numTris << " " << _rotation << endl;
	}

	//cout << fft[0] << endl;
	
	//wobblyMove();

	/*for(int i = 0;i < numBand; i++){
		cout << fft[i] << endl;
	}*/

	for(int i = 0;i < numBand; i++){		
		fftSmoothed[i] *= 0.96f;
		if (fftSmoothed[i] < fft[i]) fftSmoothed[i] = fft[i];
	}
	
	//distortion = fftSmoothed[0] * 5;
	
	updateMesh();

#ifdef USE_FILTERS
	camFilter.begin();
	TS_START("draw mesh");
	drawMain();
	TS_STOP("draw mesh");

	subViewport();
	camFilter.end();
#endif

	if(music.isLoaded() && music.getIsPlaying() && nStep == 3){
		float pos = music.getPositionMS();
		if(pos > mStartPos + mSlice){
			//music.setPositionMS(mStartPos);
			
			nStep = 4;
			music.stop();

			//take shot!
			cout << "Hand's up!" << endl;
			keyReleased(OF_KEY_F1);
			
			gui_music->setVisible(true);
			gui_music->setPosition((sw - sh*0.5) - 206, sh - 50);
			btn_music->setLabelText("SAVE VIBES");
			btn_music->setName("vibes");
		}
	}
}

//--------------------------------------------------------------
void testApp::draw(){
	if(!bPause) _rotation += ofGetLastFrameTime() * 5;	
	if(_rotation > 360) _rotation = 0;

#ifndef USE_FILTERS
	TS_START("draw mesh");
	highresScreen->begin();
	if(bGenerated) drawMain();
	highresScreen->end();
	TS_STOP("draw mesh");
	
	subViewport();
	//if(!bTakeShot) grid->draw(4, 4, ofColor(200));
#else
	ofSetColor(255);
	camFilter.draw();
#endif

	switch (nStep){
	case 0:
		if(bLocalImg && bLocationImg && bMusic){
			gui_music->setPosition((sw - sh * 0.5) * 0.5 - 100, sh * 0.5 - 20);
			btn_music->setLabelText("Generate");
			btn_music->setName("generate");
		}else{
			ofSetColor(255);
			if(bLocationImg) ofSetColor(0,255,0);
			font.drawString("1. Enter a location name.", (sw-sh*0.5) * 0.5 - 100 + 4, sh * 0.5 - 100);

			ofSetColor(255);
			if(bLocalImg) ofSetColor(0,255,0);
			font.drawString("2. Select a local image.", (sw-sh*0.5) * 0.5 - 100, sh * 0.5 - 100 + 20);

			ofSetColor(255);
			if(bMusic) ofSetColor(0,255,0);
			font.drawString("3. Select a music.", (sw-sh*0.5) * 0.5 - 100, sh * 0.5 - 100 + 40);
		}
		break;
	case 1:
		ofSetColor(colorBlue * 4);
		font.drawString("PROCESSING...", 500, 100);

		uMax += (endValues["uMax"] - uMax) * ofGetLastFrameTime() * 0.3;
		vMax += (endValues["vMax"] - vMax) * ofGetLastFrameTime();
		distortion += (endValues["distortion"] - distortion) * ofGetLastFrameTime() * 0.3;

		if(ofGetFrameNum() % 10 == 0){
			ofSeedRandom();
			
			if(ofRandomuf() > ofRandomuf()) cout << "Calculating Vertex - X: " << (PI - uMax) << " Y: " << ofRandom(-PI, PI) << " Z: " << ofRandom(-PI, PI) << endl;
			else cout << "Generating TexCoords - X: " << ofRandom(-TWO_PI, TWO_PI) << " Y: " << ofRandom(-PI, PI) << endl;
		}

		if(uMax >= endValues["uMax"] * 0.9){
			uMax = endValues["uMax"];
			vMax = endValues["vMax"];
			distortion = endValues["distortion"];

			nStep = 2;
		}
		//if(vMax >= -0.01f) vMax = 0;

		//rotateCam(cam, 0.75, 1, 0);

		break;
	case 2: 
		{
		ofSetColor(colorBlue * 4);
		font.drawString("PROCESSING...", 500, 100);

		float _t = ofGetLastFrameTime();

		tmp += (endValues["numTris"] - tmp) * _t * 0.5;
		xRange += (endValues["xRange"] - xRange) * _t;
		yRange += (endValues["yRange"] - yRange) * _t;
		zRange += (endValues["zRange"] - zRange) * _t;

		numTris = tmp;

		if(ofGetFrameNum() % 10 == 0){
			ofSeedRandom();
			cout << "Generating Triangle Path - X: " << ofRandom(-40, 40) << " Y: " << ofRandom(-40, 40) << " Z: " << ofRandom(-40, 40) << endl;
		}

		if(tmp >= endValues["numTris"] * 0.9 && xRange >= endValues["xRange"] * 0.9){
			tmp = endValues["numTris"];
			
			numTris = endValues["numTris"];
			xRange = endValues["xRange"];
			yRange = endValues["yRange"];
			zRange = endValues["zRange"];

			//distortion = ofMap(avgMid, 0.0, 2, 0.0, 15.0, false);
			args = ofMap(avgMid, 0.01, 2, 0.5, 2, true);
			if(avgLow > 1.5) kaleidoSegments = 3;

			cout << "args: " << args<< endl;

			nStep = 3;
			bProcessing = false;
			music.setPositionMS(mStartPos);
		}
		
		//rotateCam(cam, 1, 0.75, 0);

		break;
		}
	case 3:
		{
			int _n = (mStartPos + mSlice - music.getPositionMS()) / 1000;
			string _s = "Generation stops in: T - " + ofToString(_n) + "sec";

			ofSetColor(0,255,0);
			font.drawString(_s, 500, 100);
			break;
		}
	default:
		ofSetColor(colorBlue * 4);
		if(!bTakeShot) font.drawString("Image Generation Completed.", 500, 100);

		break;		
	}
}

void testApp::updateMesh(){
	kaleido->setSegments(kaleidoSegments);

	mat.setShininess(shininess);
	mat.setSpecularColor(specularIntensity);

	TS_START("update mesh");
	geom.setValues(offset, uMin, uMax, vMin, vMax, distortion, uJitter, vJitter, uChance, vChance);
	geom.setArg(args);
	geom.calculate();
	TS_STOP("update mesh");

	TS_START("tris calc");
	tris.setValues(numTris, xVar, yVar, zVar, xRange, yRange, zRange);
	if(drawTrisStripe)
		tris.calculateVertices(OF_PRIMITIVE_TRIANGLE_STRIP, true);
	else
		tris.calculateVertices(OF_PRIMITIVE_TRIANGLES, true);
	TS_STOP("tris calc");
}

void testApp::drawMain(){
	if(!bShowTex) light.enable();

	if(bDarkBg){
		ofBackground(colorDarkBlue);
		ofBackgroundGradient(colorBlue, colorDarkBlue, OF_GRADIENT_CIRCULAR);
	}else{
		ofBackground(blackness-30);
		ofBackgroundGradient(blackness, blackness-30, OF_GRADIENT_CIRCULAR);
	}

	if(!bKaleido) cam.begin(ofRectangle(0, 0, sw - sh * 0.5, sh));

	ofEnableDepthTest();
	ofDisableAlphaBlending();

	//drama begins
	if(bKaleido){
		postfx.begin(cam);
		
		if(!bDarkBg) ofBackgroundGradient(blackness, blackness-30, OF_GRADIENT_CIRCULAR);
		else ofBackgroundGradient(colorBlue, colorDarkBlue, OF_GRADIENT_CIRCULAR);

		//if(bTakeShot) ofBackground(0,0);
		ofBackground(0,0);
	}

	if(bBlendADD) ofEnableBlendMode(OF_BLENDMODE_ADD);
	if(bBlendMultiply) ofEnableBlendMode(OF_BLENDMODE_MULTIPLY);

	mat.begin();

	//glEnable(GL_BLEND);
	//glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_ONE_MINUS_SRC_COLOR);

	ofPushMatrix();
	ofRotateY(_rotation);
	
	if(!bPause) _rotation += ofGetLastFrameTime() * 2;
	ofRotateX(_rotation);

	geom.draw(bPolyRenderMode, bDrawMode, bShowTex);

	mat.end();

	ofEnableAlphaBlending();
	tris.draw(OF_MESH_FILL);
	ofDisableAlphaBlending();

	ofPopMatrix();

	//glDisable(GL_BLEND);

	if(bBlendADD) ofDisableBlendMode();
	if(bBlendMultiply) ofDisableBlendMode();

	if(bKaleido) postfx.end();

	ofDisableLighting();
	ofDisableDepthTest();

	if(!bKaleido) cam.end();
}

void testApp::subViewport(){
	ofSetColor(255);

	ofEnableDepthTest();
	//ofDisableDepthTest();
	//ofEnableAlphaBlending();

	int half = sh * 0.5;

	if(bProcessing){
		/*ofSetColor(colorDarkBlue);
		ofRect(sw - half + 4, 40, half-8, half - 8);
		ofRect(sw - half + 4, half + 40, half-8, half - 8);*/

		ofSetColor(colorBlue * 3);
		font.drawString("Processing...", sw - half * 0.5 - 50, half * 0.5);
		font.drawString("Processing...", sw - half * 0.5 - 50, half + half * 0.5 + 40);
	}
	
	
	TS_START("scam 1: draw mesh");
	ofRectangle rec1(sw - half, 0, half, half);
	scam1.begin(rec1);
	
	ofPushMatrix();
	ofRotateY(_rotation);
	ofRotateX(_rotation);

	if(bLocalImg) geom.draw(bPolyRenderMode, bDrawMode, bShowTex);

	ofPopMatrix();
	scam1.end();

	TS_STOP("scam 1: draw mesh");

	TS_START("scam 2: draw tris");

	ofRectangle rec2(sw - half, half, half, half);
	scam2.begin(rec2);

	ofPushMatrix();
	ofRotateY(_rotation);
	ofRotateX(_rotation);

	if(bLocationImg) tris.draw(OF_MESH_FILL);

	ofPopMatrix();

	scam2.end();

	TS_STOP("scam 2: draw tris");

	//ofDisableAlphaBlending();	
	ofDisableDepthTest();

	//equalizer
	if(bMusic){
		float width = (float)(5*128) / numBand;

		ofPushMatrix();
		ofTranslate((sw-half) * 0.5 - numBand * width * 0.5, 100);

		ofSetColor(200);
		for (int i = 0; i < numBand; i++){
			ofNoFill();
			ofRect(i*width, sh-100, width, -(fftSmoothed[i] * 200));
			ofFill();
		}

		ofPopMatrix();
	}

	//draw thumb 1
	float aspect = tex.getHeight() / tex.getWidth();

	ofSetColor(colorBlue * 1.5);
	ofRect(sw - 100-4, half - (100 * aspect)-4, 100 + 2, 100 * aspect + 2);

	ofSetColor(255);
	if(tex.isAllocated()) tex.draw(sw - 100 - 3, half - (100 * aspect) - 3, 100, 100 * aspect);
	
	//draw thumb 2
	aspect = trisImage.getHeight() / trisImage.getWidth();
	
	ofSetColor(colorBlue * 1.5);
	ofRect(sw - 100-4, sh - (100 * aspect)-4, 100 + 2, 100 * aspect + 2);
	
	ofSetColor(255);
	if(trisImage.isAllocated()) trisImage.draw(sw - 100 - 3, sh - (100 * aspect) - 3, 100, 100 * aspect);

	//border
	ofSetColor(colorBlue * 1.5);
	ofLine(sw-half, 0, sw-half, sh);
	ofLine(sw-half, half, sw, half);
}

void testApp::guiEvent(ofxUIEventArgs &e){
	string name = e.getName();

	if(name == "location" && !bSearching){
		ofxUITextInput *input = (ofxUITextInput *) e.widget;
		
		if(input->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_FOCUS){
			if(input->getTextString() == "enter location"){
				input->setTextString("");
				input->setFocus(true);
			}

			btn_web->setLabelText(UI_BUTTON_WEB);
		}

		if(input->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_ENTER){
			//load image
			searchLocation(txt_location->getTextString());
			btn_web->setLabelText("Searching...");
			bSearching = true;
		}

	}else if(name == UI_BUTTON_LOCAL){
		ofxUILabelButton *btn = (ofxUILabelButton *) e.widget;

		if(!btn->getValue()){
			bOpenMusic = false;
			openFile();
		}

	}else if(name == UI_BUTTON_WEB && !bSearching){
		ofxUILabelButton *btn = (ofxUILabelButton *) e.widget;
		
		if(!btn->getValue()){
			string txt = txt_location->getTextString();

			if(txt != ""){
				//load image
				searchLocation(txt);
				btn_web->setLabelText("Searching...");
				bSearching = true;
			}
		}
	}else if(name == UI_BUTTON_MUSIC){
		ofxUILabelButton *btn = (ofxUILabelButton *) e.widget;

		if(!btn->getValue()){
			bOpenMusic = true;
			openFile();
		}
	}else if(name == "generate"){
		ofxUILabelButton *btn = (ofxUILabelButton *) e.widget;

		if(!btn->getValue()){
			bProcessing = true;
			bGenerated = true;
			gui_music->setVisible(false);
			nStep = 1;

			string str("spells/10" + ofToString(spell));
			str += ".xml";
			ofxRemoteUIServer::instance()->loadFromXML(str);

			distortion = 0;
			uMax = -PI;
			vMax = -PI;

			numTris = 0;
			xRange = 0;
			yRange = 0;
			zRange = 0;

			endValues["distortion"] = 0;
			endValues["uMax"] = PI;
			endValues["vMax"] = 0;
			endValues["numTris"] = 50;
			endValues["xRange"] = 200;
			endValues["yRange"] = 200;
			endValues["zRange"] = 200;

			xParams.pushTag("OFX_REMOTE_UI_PARAMS");			
			int len = xParams.getNumTags("REMOTEUI_PARAM_FLOAT") + xParams.getNumTags("REMOTEUI_PARAM_BOOL");
			for(int i = 0; i < len; i++){
				string name = xParams.getAttribute("REMOTEUI_PARAM_FLOAT", "paramName", "", i);
				
				if(name == "distortion"){
					endValues["distortion"] = xParams.getValue("REMOTEUI_PARAM_FLOAT", 0.0, i);
					endValues["distortion"] = ofMap(avgMid, 0.0, 2, 0.0, 15.0, false);
				}else if(name == "uMax"){
					float u = xParams.getValue("REMOTEUI_PARAM_FLOAT", PI, i);
					if(u < 0) u *= -1;
					endValues["uMax"] = u;
				}else if(name == "vMax"){
					float v = xParams.getValue("REMOTEUI_PARAM_FLOAT", 0.0, i);
					if(v < 0) v *= -1;
					endValues["vMax"] = v;
				}else if(name == "numTris"){
					endValues["numTris"] = xParams.getValue("REMOTEUI_PARAM_FLOAT", 50.0, i);
				}else if(name == "xRange"){
					endValues["xRange"] = xParams.getValue("REMOTEUI_PARAM_FLOAT", 200.0, i);
				}else if(name == "yRange"){
					endValues["yRange"] = xParams.getValue("REMOTEUI_PARAM_FLOAT", 200.0, i);
				}else if(name == "zRange"){
					endValues["zRange"] = xParams.getValue("REMOTEUI_PARAM_FLOAT", 200.0, i);
				}
			}
		}
	}else if(name == "vibes"){
		ofxUILabelButton *btn = (ofxUILabelButton *) e.widget;
		
		if(!btn->getValue()){
			cout << "open vibes" << endl;
			system("start E://zz//vibes");
		}
	}
}

void testApp::openFile(){
	string str = "Select a jpg or png";
	if(bOpenMusic) str = "Select a music";

	ofFileDialogResult openFileResult = ofSystemLoadDialog(str);

	if(openFileResult.bSuccess) {
		cout << "successfully open" << endl;
		processOpenedFile(openFileResult);
	} else {
		cout << "action cancelled" << endl;
	}
}

void testApp::processOpenedFile(ofFileDialogResult result){
	ofFile file(result.getPath());

	if(file.exists()){
		string fileExtension = ofToUpper(file.getExtension());

		if ((fileExtension == "JPG" || fileExtension == "PNG") && !bOpenMusic){
			//ofLoadImage(tex, result.getPath());
			tex.clear();
			tex.loadImage(result.getPath());
			geom.setTexture(tex);
			
			bLocalImg = true;

			//if(highresScreen->steps == 4) highresScreen->init(5, 0, true);
			//else highresScreen->init(4, 0, true);
		}else if(fileExtension == "MP3" || fileExtension == "WAV"){
			music.loadSound(result.getPath(), true);
			bMusic = true;
			music.play();

			mSlice = ofRandom(10 * 1000, 20 * 1000);
			mStartPos = ofRandom(5*1000, 40*60*1000);
			music.setPositionMS(mStartPos);
		}
	}
}

void testApp::setupRemoteNodes(){
	OFX_REMOTEUI_SERVER_SETUP(15972);
	
	RUI_SHARE_PARAM(blackness, 20, 255);
	/*RUI_SHARE_PARAM(vh, 2, 358);
	RUI_SHARE_PARAM(vs, 5, 95);
	RUI_SHARE_PARAM(vb, 5, 95);
	
	RUI_SHARE_PARAM(shininess, 0, 255);
	RUI_SHARE_PARAM(specularIntensity, 0, 1);	*/

	RUI_NEW_GROUP("ETC");
	//RUI_NEW_GROUP(" ");
	RUI_SHARE_PARAM(alpaa, 0, 255);
	RUI_SHARE_PARAM(kaleidoSegments, 0, 20);
	//RUI_SHARE_PARAM(bBlendADD);
	//RUI_SHARE_PARAM(bBlendMultiply);
	RUI_SHARE_PARAM(bShowTex);
	RUI_SHARE_PARAM(bKaleido);
	RUI_SHARE_PARAM(bDarkBg);

	RUI_NEW_GROUP("Geometry Params");
	RUI_SHARE_PARAM(args, -TWO_PI, TWO_PI);
	RUI_SHARE_PARAM(args2, -TWO_PI, TWO_PI);
	RUI_SHARE_PARAM(distortion, 0, 200);
	RUI_SHARE_PARAM(uMin, -50, 50);
	RUI_SHARE_PARAM(uMax, -50, 50);
	RUI_SHARE_PARAM(vMin, -50, 50);
	RUI_SHARE_PARAM(vMax, -50, 50);
	RUI_SHARE_PARAM(uJitter, 0, 100);
	RUI_SHARE_PARAM(vJitter, 0, 100);
	RUI_SHARE_PARAM(uChance, 0, 100);
	RUI_SHARE_PARAM(vChance, 0, 100);

	OFX_REMOTEUI_SERVER_SHARE_PARAM(bDrawMode);
	OFX_REMOTEUI_SERVER_SHARE_PARAM(bPolyRenderMode);

	RUI_NEW_GROUP("Scattered Triangles");
	RUI_SHARE_PARAM(drawTrisStripe);
	RUI_SHARE_PARAM(numTris, 0, 2000);
	
	RUI_SHARE_PARAM(xVar, 0, 50);
	RUI_SHARE_PARAM(yVar, 0, 50);
	RUI_SHARE_PARAM(zVar, 0, 50);
	
	RUI_SHARE_PARAM(xRange, 0, 800);
	RUI_SHARE_PARAM(yRange, 0, 800);
	RUI_SHARE_PARAM(zRange, 0, 800);
}

// fix abnormals
void testApp::setNormals(ofMesh &mesh) {
	//The number of the vertices
	int nV = mesh.getNumVertices();
	
	//The number of the triangles
	int nT = mesh.getNumIndices() / 3;
	vector<ofPoint> norm( nV ); //Array for the normals
	
	//Scan all the triangles. For each triangle add its
	//normal to norm's vectors of triangle's vertices
	for (int t=0; t<nT; t++) {
		//Get indices of the triangle t
		int i1 = mesh.getIndex( 3 * t );
		int i2 = mesh.getIndex( 3 * t + 1 );
		int i3 = mesh.getIndex( 3 * t + 2 );
		//Get vertices of the triangle
		const ofPoint &v1 = mesh.getVertex( i1 );
		const ofPoint &v2 = mesh.getVertex( i2 );
		const ofPoint &v3 = mesh.getVertex( i3 );
		//Compute the triangle's normal
		ofPoint dir = ( (v2 - v1).crossed( v3 - v1 ) ).normalized();
		//Accumulate it to norm array for i1, i2, i3
		norm[ i1 ] += dir;
		norm[ i2 ] += dir;
		norm[ i3 ] += dir;
	}
	//Normalize the normal's length
	for (int i=0; i<nV; i++) {
		norm[i].normalize();
	}
	//Set the normals to mesh
	mesh.clearNormals();
	mesh.addNormals( norm );
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){
	//if(key == 'v'){
	////	if (vidRecorder.isRecording()) vidRecorder.stop();
	////	else vidRecorder.record();
	//	//ShellExecute("E://zz//vibes");
	//	system("start E://zz//vibes");
	//}

	if(key == OF_KEY_F2){
		tris.saveModel();
	}

	if(key == OF_KEY_F3){
		geom.saveModel();
	}

	if(key == OF_KEY_F1){
		TIME_SAMPLE_SET_ENABLED(false);

		bMusic = false;
		bTakeShot = true;

		update();
		draw();

		string name = "CM" + ofToString(ofGetUnixTime()) + ".png";
		name = "E:/zz/Vibes/" + name;
		screenImage.allocate(sw - sh * 0.5, sh, OF_IMAGE_COLOR_ALPHA);

		glReadBuffer(GL_COLOR_ATTACHMENT0_EXT);
		glReadPixels(0, 0, sw - sh * 0.5, sh, GL_RGBA, GL_UNSIGNED_BYTE, screenImage.getPixels());
		screenImage.flipY();
		
		screenImage.saveImage(name, OF_IMAGE_QUALITY_BEST);
		
		bMusic = true;
		bTakeShot = false;
		bShotTaken = true;

		//TIME_SAMPLE_SET_ENABLED(true);
	}

	//if(key == 'h' || key == 'H'){
	//	bPause = true;
	//	noiseWarp->setSpeed(0);

	//	update();		
	//	draw();

	//	string name = "HighRes/CM_HIGH" + ofToString(ofGetUnixTime()) + ".png";
	//	highresScreen->finish(name, true);
	//	
	//	bPause = false;
	//	noiseWarp->setSpeed(0.1);
	//}
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
	
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){
	sw = w;
	sh = h;

	initPostFx();
	
	gui_music->clearWidgets();
	gui_web->clearWidgets();
	gui_local->clearWidgets();
	initGUIElements();

	//gui_local->setPosition(sw - sh * 0.5, 0);
	//gui_local->setDimensions(sh * 0.5, sh * 0.5);
	//gui_web->setPosition(sw - sh * 0.5, sh * 0.5);
	//gui_web->setDimensions(sh * 0.5, sh * 0.5);

	grid = new UIGrid(sw - sh * 0.5 - 8, sh);
}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}

void testApp::exit(){
//	tris.stopThread();
	loader.exit();

	ofRemoveListener(gui_local->newGUIEvent, this, &testApp::guiEvent);
	ofRemoveListener(gui_web->newGUIEvent, this, &testApp::guiEvent);
	ofRemoveListener(loader.event, this, &testApp::responseCallback);
	ofRemoveListener(loader.eventImage, this, &testApp::setWebPhoto);
}

void testApp::wobblyMove(){
	_count++;
	_time += ofGetLastFrameTime();

	if(speedUp){
		float next = noiseWarp->getSpeed();

		//	cout << (dest - next) << endl;

		if(dest > next)
			next += abs(next - dest) / 50;
		else next -= abs(dest - next) / 50;

		noiseWarp->setSpeed(next);

		if( abs(dest - next) < 0.01)
			speedUp = false;
	}

	if(_time > 4.0f){
		mean = mean / (_count * numBand);

		dest = mean * 3;
		speedUp = true;

		cout << mean << endl;

		_time = 0;		
		_count = 0;
		mean = 0;
	}
}

void testApp::rotateCam(ofEasyCam &camera, float xRot, float yRot, float zRot){
	ofQuaternion curRot = ofQuaternion(5, camera.getXAxis(), 0, camera.getYAxis(), 3, camera.getZAxis());
	camera.setPosition((camera.getGlobalPosition()-camera.getTarget().getGlobalPosition())*curRot + camera.getTarget().getGlobalPosition());
	camera.rotate(curRot);
}

void testApp::initGUIElements(){
	int half = sh * 0.5;

	gui_music = new ofxUICanvas(6, sh - 50, 300, 50);
	gui_music->setDrawBack(false);
	gui_music->setFont("Gotham-Book.otf", true, true, false, 0.0f, 150);
	ofAddListener(gui_music->newGUIEvent, this, &testApp::guiEvent);

	btn_music = gui_music->addLabelButton(UI_BUTTON_MUSIC, false, 190, 36);
	btn_music->setColorBack(ofxUIColor(COLOR_GRAY_BLUE, 220));

	gui_local = new ofxUICanvas(sw - half, 0, half, half * 2);
	gui_local->setDrawBack(false);
	ofAddListener(gui_local->newGUIEvent, this, &testApp::guiEvent);
	gui_local->setFont("Gotham-Book.otf", true, true, false, 0.0f, 150);
	//gui_local->setWidgetFontSize(OFX_UI_FONT_SMALL);

	gui_web = new ofxUICanvas(sw - half, half, half, half * 2);
	gui_web->setDrawBack(false);
	ofAddListener(gui_web->newGUIEvent, this, &testApp::guiEvent);
	gui_web->setFont("Gotham-Book.otf", true, true, false, 0.0f, 150);
	//gui_web->setFontSize(OFX_UI_FONT_MEDIUM, 16, 300);

	ofxUILabelButton *btn_local = gui_local->addLabelButton(UI_BUTTON_LOCAL, false, half - 8, 30);
	btn_local->setColorBack(ofxUIColor(COLOR_GRAY_BLUE, 220));

	txt_location = gui_web->addTextInput("location", "enter location", half - 8, 30);
	txt_location->setAutoClear(false);
	txt_location->setColorBack(ofxUIColor(COLOR_GRAY_BLUE, 220));

	btn_web = gui_web->addLabelButton(UI_BUTTON_WEB, false, half - 8, 30);
	btn_web->setColorBack(ofxUIColor(COLOR_GRAY_BLUE, 220));

	ofAddListener(loader.event, this, &testApp::responseCallback);
	ofAddListener(loader.eventImage, this, &testApp::setWebPhoto);
}

void testApp::initMusic(){
	avgHigh = 0;
	avgLow = 0;
	avgMid = 0;

	fftSmoothed = new float[8192];
	for (int i = 0; i < 8192; i++){
		fftSmoothed[i] = 0;
	}

	bSampled = false;
	music.setVolume(1.0);
	numBand = 64;
	//music.loadSound("musics/DJsarhi - Levitate.mp3");
	music.setLoop(true);	
//	music.play();
//	float sp = ofRandom(5*1000, 7*60*1000);	
//	music.setPositionMS(sp);
}

void testApp::searchLocation(string loc){
	//string place_url = "https://api.flickr.com/services/rest/?method=flickr.places.find&api_key=8cc4e7e51b9b0f8adf7f807c0547d0c3&query=" + loc;
	loader.fetchLocation(loc);
}

void testApp::setWebPhoto(string &path){
	//trisImage.loadImage("flickr/6084219302_857a267f05.jpg");
	//trisImage.loadImage("5.jpg");
	trisImage.loadImage(path);
	tris.addImage(trisImage);
	tris.calculateVertices(OF_PRIMITIVE_TRIANGLES, true);

	btn_web->setLabelText(UI_BUTTON_WEB);
	bSearching = false;
	bLocationImg = true;
}

void testApp::responseCallback(string &e){
	btn_web->setLabelText(e);
	bSearching = false;
	//string s = "bleh";
	//setWebPhoto(s);
}

void testApp::initVariables(){
	//Mesh Geometry Params
	xCount = 160;
	yCount = 160;
	offset = 50;
	uMin = -PI;
	uMax = PI;
	vMin = -PI;
	vMax = 30;
	distortion = 0;
	uJitter = 0;
	vJitter = 0;
	uChance = 40;
	vChance = 40;

	//Mesh Color Params - hue, sat, brightness
	vh = 190;
	vs = 90;
	vb = 85;

	//tex
	//ofLoadImage(tex, "geo.jpg");
	//tex.loadImage("geo.jpg");
	//geom.setTexture(tex);

	//Fill background
	blackness = 240;
	alpaa = 255;

	//Gradient background
	colorDarkBlue.set(15,15,18);
	colorBlue.set(44,50,64);

	//Tris paramas
	numTris = 50;
	xVar = 25;
	yVar = 25;
	zVar = 25;
	xRange = 200;
	yRange = 200;
	zRange = 200;

	//Mat params
	shininess = 20;
	specularIntensity = 0.3;

	//boos
	bBlendADD = false;
	bBlendMultiply = false;
	bShowTex = true;
	bDrawMode = true;			//fill vs wireframe mode
	bPolyRenderMode = true;	//triangles vs triangle_stripe mode
	drawTrisStripe = true;	//tris stripe
	bKaleido = true;			//mesmerizing
	bDarkBg = true;			//dark vs light bg
	bSampled = false;

	kaleidoSegments = 2;

	colorLight = ofFloatColor::white;
	colorLight.setBrightness(250);

	light.setDirectional();	
	light.setOrientation(ofVec3f(0,200,0));
	light.setDiffuseColor(colorLight);	

	//trisImage.loadImage("tris.jpg");
	//tris.addImage(trisImage);
	tris.setValues(numTris, xVar, yVar, zVar, xRange, yRange, zRange);
	tris.calculateVertices(OF_PRIMITIVE_TRIANGLES, true);
	//tris.startThread();

	scam1.disableMouseInput();
	scam1.disableMouseMiddleButton();
	scam1.setDistance(800);

	scam2.disableMouseInput();
	scam2.disableMouseMiddleButton();
	scam2.setDistance(800);

	setupRemoteNodes();
}